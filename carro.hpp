#include <iostream>
#include <string>

using namespace std;

class Carro{
   //Atrbutos
   private:
   int numeroDePortas;
   float velocidade;
   float fatorDeFrenagem;
   float fatorDeAceleracao;
   string chassis;
   string cor;
   string modelo;
   string fabricante;
   string estado;
   bool nuts;
public:
   //Metodos
   Carro();
   Carro(string fabricante);
   Carro(string fabricante, string cor);
   ~Carro();
   //Metodos Acessores
   string getChassis();
   void setChassis(string chassis);
   string getCor();
   void setCor(string cor);
   int getNumeroDePortas();
   void setNumeroDePortas(int numeroDePortas);
   string getModelo();
   void setModelo(string modelo);
   string getFabricante();
   void setFabricante(string fabricante);
   string getEstado();
   void setEstado(string estado);
   float getVelocidade(); 
   void setVelocidade(float velocidade);
   //Outros Metodos
   void ligar();
   void desligar();
   void acelerar(float fatorDeAceleracao);
   void frear(float fatorDeFrenagem);
};