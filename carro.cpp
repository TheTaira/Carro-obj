#include "carro.hpp"
#include <string>
using namespace std;

Carro::Carro(){
    chassis = "0000";
    modelo = "padrao";
    fabricante = "generico";
    estado = "estacionado";
    velocidade = 0.0;
    numeroDePortas = 2;
    cor = "qualquer";

    cout <<"O Carro foi criado"<< endl;
}

Carro::Carro(string fabricante){
    chassis = "0000";
    modelo = "rico";
    this->fabricante = fabricante;
    estado = "estacionado";
    velocidade = 0.0;
    numeroDePortas = 2;
    cor = "qualquer";

    cout <<"O Carro caro foi criado"<< endl;
}

Carro::Carro(string fabricante, string cor){
    chassis = "0000";
    modelo = "rico";
    this->fabricante = fabricante;
    estado = "estacionado";
    velocidade = 0.0;
    numeroDePortas = 2;
    this->cor = cor;

    cout <<"O Carro caro e vermelho foi criado"<< endl;
}

Carro::~Carro(){
    cout << "O Carro foi destruido"<< endl;
}
string Carro::getChassis(){
    return chassis;
}
void Carro::setChassis(string chassis){
    this->chassis = chassis; 

}
string Carro::getCor(){
    return cor;
}

void Carro::setCor(string cor){
    this->cor = cor;
}

int Carro::getNumeroDePortas(){
    return numeroDePortas;
}
void Carro::setNumeroDePortas(int numeroDePortas){
    this->numeroDePortas = numeroDePortas;
}
string Carro::getModelo(){
    return modelo;
}
void Carro::setModelo(string modelo){
    this->modelo = modelo;
}
string Carro::getFabricante(){
    return fabricante;
}
void Carro::setFabricante(string fabricante){
    this->fabricante = fabricante;
}
string Carro::getEstado(){
    return estado;
}
void Carro::setEstado(string estado){
    this->estado = estado;
}
float Carro::getVelocidade(){
    return velocidade;
}
void Carro::setVelocidade(float velocidade){
    this->velocidade=velocidade;
    }
void Carro::ligar(){
    cout << "carro ligado"<< endl;
    setEstado("estacionado e ligado");
}
void Carro::desligar(){
    cout << "carro desligado" << endl;
    setEstado("estacionado e desligado");
    setVelocidade(0.0);
}
void Carro::acelerar(float fatorDeaceleracao){
    float velocidadeAtual = getVelocidade();
    setVelocidade(velocidadeAtual+fatorDeAceleracao);
}
void Carro::frear(float fatorDeFrengem){
    this->fatorDeFrenagem = fatorDeFrenagem;
}
